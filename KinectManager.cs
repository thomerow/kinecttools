﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectTools
{
    public class KinectManager
    {
        private object _lockObject = new object();
        private bool _isRunning = false;

        public KinectSensor Kinect { get; private set; }

        public void Start()
        {
            if (_isRunning) return;

            lock (_lockObject)
            {
                if (!_isRunning)
                {
                    _isRunning = true;
                    KinectSensor.KinectSensors.StatusChanged += KinectSensors_StatusChanged;
                    TryFindAndStartKinect();
                }
            }
        }

        public void Stop()
        {
            if (!_isRunning) return;

            lock (_lockObject)
            {
                if (_isRunning)
                {
                    _isRunning = false;
                    KinectSensor.KinectSensors.StatusChanged -= KinectSensors_StatusChanged;
                    SetSensor(null);
                }
            }
        }

        public event EventHandler<KinectChangedEventArgs> KinectChanged;

        void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            if (e == null) return;
            if ((e.Sensor == this.Kinect) || (this.Kinect == null)) 
            {
                lock (_lockObject)
                {
                    if ((e.Sensor == this.Kinect) || (this.Kinect == null))
                    {
                        TryFindAndStartKinect();
                    }
                }
            }
        }

        private void TryFindAndStartKinect()
        {
            if (!_isRunning) return;

            if ((Kinect != null) && (Kinect.Status == KinectStatus.Connected))
            {
                // We already have a connected sensor.
                return;
            }

            KinectSensor newSensor = null;

            if (KinectSensor.KinectSensors.Count != 0)
            {
                foreach (var sensor in KinectSensor.KinectSensors)
                {
                    if (sensor.Status != KinectStatus.Connected) continue;
                    if (sensor.IsRunning) continue;
                    try { sensor.Start(); } catch { continue; }
                    newSensor = sensor; // New sensor found
                    break;
                }
            }

            SetSensor(newSensor);
        }

        private void SetSensor(KinectSensor newSensor)
        {
            KinectSensor oldSensor = Kinect;
            if (newSensor == oldSensor) return;

            if (oldSensor != null) oldSensor.Stop();
            Kinect = newSensor;
            OnKinectChanged(oldSensor, newSensor);
        }

        protected virtual void OnKinectChanged(KinectSensor oldSensor, KinectSensor newSensor)
        {
            if (KinectChanged == null) return;
            KinectChanged(this, new KinectChangedEventArgs(oldSensor, newSensor));
        }
    }
}
